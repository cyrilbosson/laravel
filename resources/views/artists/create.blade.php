<x-layout>

    <x-slot name="title">Create an artist</x-slot>

    <form method="POST" action="{{ route('artist.store') }}" enctype="mutl">
        {{ csrf_field() }}
        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="" required />
        </p>
        <p>
            <label for="firstname">Firstname</label>
            <input type="text" name="firstname" id="firstname" value="" required />
        </p>
        <p>
            <label for="country_id">Country</label>
            <select name="country_id" id="country_id" required>
                @foreach($countries as $country)
                <option value="{{ $country->id }}">
                    {{ $country->name }}
                </option>
                @endforeach
            </select>
        </p>
        <p>
            <label for="photo">Photo</label>
            <input type="input" name="photo" id="photo" value="" required />
        </p>
        <button type="submit">Submit</button>
    </form>
</x-layout>
