<x-layout>
    <x-slot name="title">List of actors</x-slot>

    <a href="{{ route('artist.create') }}" class="btn mb-4">Add new</a>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">{{ __('Name') }}</th>
                <th scope="col" class="px-6 py-3">{{ __('Firstname') }}</th>
                <th scope="col" class="px-6 py-3">{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($artists as $artist)
            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">{{ $artist->name }}</th>
                <th class="px-6 py-4">{{ $artist->firstname }}</th>
                <th class="px-6 py-4 table-action">
                    <a href="{{ route('artist.edit', $artist->id) }}" class="btn">Edit</a>
                    <a href="{{ route('artist.destroy', $artist->id) }}" class="btn btn--alert delete">Delete</a>
                </th>
            </tr>
        @endforeach
        </tbody>
        </table>

    </div>

    <x-slot:script>
        <script>
        // Récupération du token
        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        // Ajout des événements
        document.querySelectorAll('.delete').forEach(item => {
          item.addEventListener('click', event => {
            event.preventDefault();

            // Requête AJAX de suppression
            fetch(event.target.href, {
              headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': token
              },
              method: 'DELETE',
            });
          })
        });
        </script>
    </x-slot:script>

</x-layout>
