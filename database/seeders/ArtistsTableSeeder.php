<?php

namespace Database\Seeders;

use App\Models\Artist;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('artists')->insert([[
            'name' => 'Coppola',
            'firstname' => 'Francis Ford',
            'birthdate' => 1939,
        ],[
            'name' => 'Lynch',
            'firstname' => 'David',
            'birthdate' => 1946,
        ]]);*/

        Artist::factory()
              ->count(50)
              ->create();
    }
}
